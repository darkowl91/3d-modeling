package modeling.shape;

import modeling.shape.model.FacePoint;
import modeling.shape.model.ShapeFace;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Soldier
 * Date: 20.10.13
 * Time: 17:32
 */
public class Cone {

    public static double H = 100;
    public static double R = 100;
    public static double n = 20;
    private static final double alpha = 360 / n;

    public static List<ShapeFace> getFaceList() {
        List<FacePoint> plane_bottom = new ArrayList<>();
        List<ShapeFace> faceList = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            plane_bottom.add(new FacePoint(R * Math.cos(Math.toRadians(i * alpha)), 0, R * Math.sin(Math.toRadians(i * alpha))));
            plane_bottom.add(new FacePoint(R * Math.cos(Math.toRadians((i ) * alpha)), 0, R * Math.sin(Math.toRadians((i ) * alpha))));

            faceList.add(new ShapeFace("body_" + i, new FacePoint(0, -H, 0),
                    new FacePoint(R * Math.cos(Math.toRadians((i+1) * alpha)), 0, R * Math.sin(Math.toRadians((i+1) * alpha))),
                    new FacePoint(R * Math.cos(Math.toRadians((i) * alpha)), 0, R * Math.sin(Math.toRadians((i) * alpha)))));
//            FACE_LIST.add(new ShapeFace(new FacePoint(0, 0, 0),
//                    new FacePoint(R * Math.cos(Math.toRadians(i * alpha)), 0, R * Math.sin(Math.toRadians(i * alpha))),
//                    new FacePoint(R * Math.cos(Math.toRadians((i + 1) * alpha)), 0, R * Math.sin(Math.toRadians((i + 1) * alpha)))));
        }
        faceList.add(new ShapeFace("bottom_0", plane_bottom));
        return faceList;
    }
}
