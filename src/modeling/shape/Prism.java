package modeling.shape;

import modeling.shape.model.FacePoint;
import modeling.shape.model.ShapeFace;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Soldier
 * Date: 20.10.13
 * Time: 17:48
 */
public class Prism {

    public static double H = 150;
    public static double R = 150;
    public static int n = 5;

    public static List<ShapeFace> getFaceList() {
        List<FacePoint> plane_top = new ArrayList<>();
        double alpha = 360 / n;
        List<FacePoint> plane_bottom = new ArrayList<>();
        List<ShapeFace> faceList = new ArrayList<>();
        for (int i = 0; i < n; i++) {

           // plane_top.add(new FacePoint(R * Math.cos(Math.toRadians(i * alpha)), -H, R * Math.sin(Math.toRadians(i * alpha))));
            //  plane_top.add(new FacePoint(R * Math.cos(Math.toRadians((i + 1) * alpha)), -H, R * Math.sin(Math.toRadians((i + 1) * alpha))));

            plane_bottom.add(new FacePoint(R * Math.cos(Math.toRadians(i * alpha)), 0, R * Math.sin(Math.toRadians(i * alpha))));
            //  plane_bottom.add(new FacePoint(R * Math.cos(Math.toRadians((i + 1) * alpha)), 0, R * Math.sin(Math.toRadians((i + 1) * alpha))));

            faceList.add(new ShapeFace("body_" + i,
                    new FacePoint(R * Math.cos(Math.toRadians(i * alpha)), -H, R * Math.sin(Math.toRadians(i * alpha))),
                    new FacePoint(R * Math.cos(Math.toRadians((i + 1) * alpha)), -H, R * Math.sin(Math.toRadians((i + 1) * alpha))),
                    new FacePoint(R * Math.cos(Math.toRadians((i + 1) * alpha)), 0, R * Math.sin(Math.toRadians((i + 1) * alpha))),
                    new FacePoint(R * Math.cos(Math.toRadians(i * alpha)), 0, R * Math.sin(Math.toRadians(i * alpha)))
            ));

        }

        for (int i = n; i > 0; i--) {

            plane_top.add(new FacePoint(R * Math.cos(Math.toRadians(i * alpha)), -H, R * Math.sin(Math.toRadians(i * alpha))));
        }
        faceList.add(new ShapeFace("top_0", plane_top));
        faceList.add(new ShapeFace("bottom_0", plane_bottom));
        return faceList;
    }
}
