package modeling.app.validation;

import javafx.scene.control.TextField;
import sun.misc.Regexp;

/**
 * User: Owl
 * Date: 19.10.13
 * Time: 18:18
 */
public class FieldValidator {
    public static final Regexp REG_EXP_DIGITS_NEGATIVE = new Regexp("^[-]?[\\d]");
    public static final Regexp REG_EXP_DIGITS = new Regexp("\\d+");
    public static final Regexp REG_EXP_DECIMAL_DIGITS = new Regexp("[\\d]+([.]?[\\d]+)?");
    public static final Regexp REG_EXP_DECIMAL_NEGATIVE = new Regexp("^[-]?[\\d]+([.]?[\\d]+)?");
    public static final Regexp REG_EXP_SCALE = new Regexp("((0.)\\d*[1-9]{1})|^([1-9]{1}\\d*([.]?[\\d]+)?)");
    public static final Regexp REG_EXP_GRAD = new Regexp("^([0-9]|[1-9]\\d|[12]\\d\\d|3[0-5]\\d|360)(\\.\\d+)?$");
    private static FieldValidator ourInstance;


    public static FieldValidator getInstance() {
        if (ourInstance == null) {
            ourInstance = new FieldValidator();
        }
        return ourInstance;
    }

    private FieldValidator() {
    }

//    public void addValidateOnTextInput(final TextField textField, final Regexp regexp) {
//        addValidateOnTextInput(textField, regexp, -1);
//    }
//
//    public void addValidateOnTextInput(final TextField textField, final Regexp regexp, final int maxLength) {
//        addValidateOnTextInput(textField, regexp, maxLength, -1);
//    }
//
//    public void addValidateOnTextInput(final TextField textField, final Regexp regexp, final int maxLength, final int minLength) {
//        ChangeListener<String> changeListener;
//        if (maxLength != -1 && minLength == -1) {
//            changeListener = new ChangeListener<String>() {
//                @Override
//                public void changed(ObservableValue<? extends String> ov, String t, String t1) {
//                    if (!validateField(textField, regexp)
//                            || (textField.textProperty().get().length() > maxLength)) {
//                        textField.clear();
//                    } else {
//                        textField.setText(t1);
//                    }
//                }
//            };
//        } else if (minLength != -1 && maxLength == -1) {
//            changeListener = new ChangeListener<String>() {
//                @Override
//                public void changed(ObservableValue<? extends String> ov, String t, String t1) {
//                    if (!validateField(textField, regexp)
//                            || (textField.textProperty().get().length() <= minLength)) {
//                        textField.clear();
//                    } else {
//                        textField.setText(t1);
//                    }
//                }
//            };
//        } else {
//            changeListener = new ChangeListener<String>() {
//                @Override
//                public void changed(ObservableValue<? extends String> ov, String t, String t1) {
//                    if (!validateField(textField, regexp)
//                            || (textField.textProperty().get().length() > maxLength)
//                            && (textField.textProperty().get().length() <= minLength)) {
//                        textField.clear();
//                    } else {
//                        textField.setText(t1);
//                    }
//                }
//            };
//        }
//        textField.textProperty().addListener(changeListener);
//    }

    public boolean validateField(TextField field, Regexp regexp) {
        return field.getText().matches(regexp.exp);
    }
}
