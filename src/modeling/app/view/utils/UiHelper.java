package modeling.app.view.utils;

import javafx.event.Event;
import javafx.scene.control.Control;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import modeling.app.validation.FieldValidator;

import java.util.List;

/**
 * User: Admin
 * Date: 02.11.13
 * Time: 15:39
 */
public class UiHelper {
    private static UiHelper ourInstance;
    private final FieldValidator fieldValidator;

    public static UiHelper getInstance() {
        if (ourInstance == null) {
            ourInstance = new UiHelper();
        }
        return ourInstance;
    }

    private UiHelper() {
        fieldValidator = FieldValidator.getInstance();
    }

    public double getAngelFormUi(Slider uiAngle, TextField uiCurrentAngle, Event event) {
        if (event.getTarget().toString().startsWith("TextField")) {
            if (!fieldValidator.validateField(uiCurrentAngle, FieldValidator.REG_EXP_GRAD)) {
                uiCurrentAngle.setText(String.format("%.0f", uiAngle.getValue()));
            }
            uiAngle.setValue(Double.valueOf(uiCurrentAngle.getText()));
        } else {
            uiCurrentAngle.setText(String.format("%.0f", uiAngle.getValue()));
        }
        return Double.valueOf(uiCurrentAngle.getText());
    }

    public double getValueAsDouble(TextField textField, double ifNull) {
        if (fieldValidator.validateField(textField, FieldValidator.REG_EXP_DECIMAL_NEGATIVE)) {
            return Double.valueOf(textField.getText());
        } else {
            textField.setText(String.format("%.0f", ifNull));
            return ifNull;
        }
    }

    public int getValueAsInt(TextField textField, int ifNull) {
        if (fieldValidator.validateField(textField, FieldValidator.REG_EXP_DIGITS)) {
            return Integer.valueOf(textField.getText());
        } else {
            textField.setText(String.valueOf(ifNull));
            return ifNull;
        }
    }

    public double getValueAsScale(TextField textField) {
        if (fieldValidator.validateField(textField, FieldValidator.REG_EXP_SCALE)) {
            return Double.valueOf(textField.getText());
        } else {
            textField.setText(String.valueOf((double) 1));
            return (double) 1;
        }
    }

    public void setDisable(boolean isVisible, List<Control> controls) {
        for (Control control : controls) {
            control.setDisable(isVisible);
        }
    }
}
