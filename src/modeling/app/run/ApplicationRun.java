package modeling.app.run;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import modeling.shapeParameters.params.ParamsHolder;

public class ApplicationRun extends Application {
    public static Stage primaryStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(ParamsHolder.getInstance().getStartUpViewPath()));
        primaryStage.setTitle(ParamsHolder.getInstance().getTitle());
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
        ApplicationRun.primaryStage = primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
