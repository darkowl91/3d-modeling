package modeling.shapeParameters.app.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import modeling.app.view.utils.UiHelper;
import modeling.shape.Cone;
import modeling.shape.Cube;
import modeling.shape.Cylinder;
import modeling.shape.Prism;
import modeling.shape.geometry.GeometryUtils;
import modeling.shape.model.Shape;
import modeling.shapeParameters.params.ParamsHolder;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * User: Owl
 * Date: 16.11.13
 * Time: 19:41
 */
public class ParametersController implements Initializable {

    @FXML
    private TextField uiCubeSize;
    @FXML
    private TextField uiPrizmHeight;
    @FXML
    private TextField uiPrizmNumberOfEdges;
    @FXML
    private TextField uiLayoutX;
    @FXML
    private TextField uiLayoutY;
    @FXML
    private ToggleGroup uiConnrctionType;
    @FXML
    private RadioButton uiBack;
    @FXML
    private RadioButton uiInfrontOf;
    @FXML
    private RadioButton uiTop;
    @FXML
    private RadioButton uiBottom;
    @FXML
    private RadioButton uiLeft;
    @FXML
    private RadioButton uiRight;
    @FXML
    private TextField uiCylinderR;
    @FXML
    private TextField uiCylinderH;
    @FXML
    private TextField uiCylinderN;
    @FXML
    private TextField uiConeR;
    @FXML
    private TextField uiConeH;
    @FXML
    private TextField uiConeN;


    private final UiHelper uiUtil = UiHelper.getInstance();
    private final ParamsHolder prmHolder = ParamsHolder.getInstance();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        String[] strings = url.toString().split("/");
        String vewName = strings[strings.length - 1];
        switch (vewName) {
            case "ShapeParametersAlya.fxml":
                uiCylinderH.setText(String.format("%.0f", Cylinder.H));
                uiCylinderN.setText(String.valueOf(Cylinder.n));
                uiCylinderR.setText(String.format("%.0f", Cylinder.R));
                uiConeH.setText(String.format("%.0f", Cone.H));
                uiConeN.setText(String.valueOf(Cone.n));
                uiConeR.setText(String.format("%.0f", Cone.R));
                break;
            case "ShapeParametersNickolay.fxml":
                uiBack.setUserData("-" + GeometryUtils.Z_AXIS);
                uiInfrontOf.setUserData(GeometryUtils.Z_AXIS);
                uiLeft.setUserData("-" + GeometryUtils.X_AXIS);
                uiRight.setUserData(GeometryUtils.X_AXIS);
                uiCubeSize.setText(String.format("%.0f", Cube.size));
                uiPrizmHeight.setText(String.format("%.0f", Prism.H));
                uiPrizmNumberOfEdges.setText(String.valueOf(Prism.n));
                break;
            case "ShapeParametersMasha.fxml":
                uiBack.setUserData("-" + GeometryUtils.Z_AXIS);
                uiInfrontOf.setUserData(GeometryUtils.Z_AXIS);
                uiLeft.setUserData("-" + GeometryUtils.X_AXIS);
                uiRight.setUserData(GeometryUtils.X_AXIS);
                uiCubeSize.setText(String.format("%.0f", Cube.size));
                uiCylinderH.setText(String.format("%.0f", Cylinder.H));
                uiCylinderN.setText(String.valueOf(Cylinder.n));
                uiCylinderR.setText(String.format("%.0f", Cylinder.R));
                break;
        }
        uiTop.setUserData(GeometryUtils.Y_AXIS);
        uiBottom.setUserData("-" + GeometryUtils.Y_AXIS);
    }

    @FXML
    public void changeCubePrismParams() {
        Shape shapeModel = prmHolder.getShapeModel();
        Cube.size = uiUtil.getValueAsInt(uiCubeSize, 100);
        shapeModel.setShapeFaces(Cube.getFaceList());
        Prism.H = uiUtil.getValueAsInt(uiPrizmHeight, 100);
        Prism.n = uiUtil.getValueAsInt(uiPrizmNumberOfEdges, 5);
        Shape SubShape = new Shape(Prism.getFaceList());
        SubShape.translate(uiUtil.getValueAsInt(uiLayoutX, 0), 0, uiUtil.getValueAsInt(uiLayoutY, 0));

        String s = uiConnrctionType.getSelectedToggle().getUserData().toString();
        if (s.charAt(0) == '-') {
            String substring = s.substring(1, s.length());
            shapeModel.addShape(SubShape, substring, -Cube.size);
        } else {
            shapeModel.addShape(SubShape, s, Cube.size);
        }
        shapeModel.resetToCurrentState();
    }

    @FXML
    public void changeCylinderConeParams() {
        Shape shapeModel = prmHolder.getShapeModel();
        Cylinder.H = uiUtil.getValueAsInt(uiCylinderH, 100);
        Cylinder.R = uiUtil.getValueAsInt(uiCylinderR, 50);
        Cylinder.n = uiUtil.getValueAsInt(uiCylinderN, 20);
        shapeModel.setShapeFaces(Cylinder.getFaceList());

        Cone.H = uiUtil.getValueAsInt(uiConeH, 100);
        Cone.R = uiUtil.getValueAsInt(uiConeR, 50);
        Cone.n = uiUtil.getValueAsInt(uiConeN, 20);
        Shape subShape = new Shape(Cone.getFaceList());
        subShape.translate(uiUtil.getValueAsInt(uiLayoutX, 0), 0, uiUtil.getValueAsInt(uiLayoutY, 0));

        String s = uiConnrctionType.getSelectedToggle().getUserData().toString();
        if (s.charAt(0) == '-') {
            String substring = s.substring(1, s.length());
            shapeModel.addShape(subShape, substring, -Cylinder.H);
        } else {
            shapeModel.addShape(subShape, s, Cylinder.H);
        }
        shapeModel.resetToCurrentState();
    }

    @FXML
    public void changeCubeCylinderParams() {
        Shape shapeModel = prmHolder.getShapeModel();
        Cube.size = uiUtil.getValueAsInt(uiCubeSize, 100);
        shapeModel.setShapeFaces(Cube.getFaceList());
        Cylinder.H = uiUtil.getValueAsInt(uiCylinderH, 100);
        Cylinder.R = uiUtil.getValueAsInt(uiCylinderR, 50);
        Cylinder.n = uiUtil.getValueAsInt(uiCylinderN, 20);
        Shape subShape = new Shape(Cylinder.getFaceList());
        subShape.translate(uiUtil.getValueAsInt(uiLayoutX, 0), 0, uiUtil.getValueAsInt(uiLayoutY, 0));

        String s = uiConnrctionType.getSelectedToggle().getUserData().toString();
        if (s.charAt(0) == '-') {
            String substring = s.substring(1, s.length());
            shapeModel.addShape(subShape, substring, -(Cube.size + Cylinder.H));
        } else {
            shapeModel.addShape(subShape, s, (Cube.size + Cylinder.H));
        }
        shapeModel.resetToCurrentState();
    }
}
