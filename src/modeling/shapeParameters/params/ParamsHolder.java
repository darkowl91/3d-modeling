package modeling.shapeParameters.params;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import modeling.shape.Cone;
import modeling.shape.Cube;
import modeling.shape.Cylinder;
import modeling.shape.Prism;
import modeling.shape.model.Shape;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * User: Owl
 * Date: 16.11.13
 * Time: 20:41
 */
public class ParamsHolder {
    private static ParamsHolder instance;
    private final Map<String, Object> parameters;
    private Properties configFile;

    public static ParamsHolder getInstance() {
        if (instance == null) {
            instance = new ParamsHolder();
        }
        return instance;
    }

    private ParamsHolder() {
        loadConfig();
        parameters = new HashMap<>();
        parameters.put("MainStageViewAlya", "/modeling/app/view/MainStageViewAlya.fxml");
        parameters.put("MainStageViewMasha", "/modeling/app/view/MainStageViewMasha.fxml");
        parameters.put("MainStageViewNickolay", "/modeling/app/view/MainStageViewNickolay.fxml");
        parameters.put("ShapeParametersNickolay", "/modeling/shapeParameters/app/view/ShapeParametersNickolay.fxml");
        parameters.put("ShapeParametersAlya", "/modeling/shapeParameters/app/view/ShapeParametersAlya.fxml");
        parameters.put("ShapeParametersMasha", "/modeling/shapeParameters/app/view/ShapeParametersMasha.fxml");
        parameters.put("Cube", new Shape(Cube.getFaceList()));
        parameters.put("Prism", new Shape(Prism.getFaceList()));
        parameters.put("Cylinder", new Shape(Cylinder.getFaceList()));
        parameters.put("Cone", new Shape(Cone.getFaceList()));
        parameters.put("lightON", new Image("/modeling/app/view/images/light-on.png"));
        parameters.put("lightOFF", new Image("/modeling/app/view/images/light-off.png"));
    }

    public double getIndent() {
        return Double.valueOf(configFile.getProperty("indent"));
    }

    public String getStartUpViewPath() {
        return (String) parameters.get(configFile.getProperty("run"));
    }

    public String getParamsViewPath() {
        return (String) parameters.get(configFile.getProperty("parameters"));
    }

    public Shape getShapeModel() {
        return (Shape) parameters.get(configFile.getProperty("mainShape"));
    }

    public Shape getSubShape() {
        return (Shape) parameters.get(configFile.getProperty("subShape"));
    }

    public String getTitle() {
        return configFile.getProperty("title");
    }

    public double getWidth() {
        return Double.valueOf(configFile.getProperty("width"));
    }

    public double getHeight() {
        return Double.valueOf(configFile.getProperty("height"));
    }

    public Color getAxisColor() {
        return Color.valueOf(configFile.getProperty("axis.color"));

    }

    public Color getGridColor() {
        return Color.valueOf(configFile.getProperty("grid.color"));
    }

    public Image getOnImage() {
        return (Image) parameters.get("lightON");
    }

    public Image getOffImage() {
        return (Image) parameters.get("lightOFF");
    }

    private void loadConfig() {
        configFile = new Properties();
        try {
            FileInputStream inputStream = new FileInputStream(new File("startup.properties"));
            configFile.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
